Partner
=======
Partner from Cornell Small Data Lab [SDL].

### Goal

- Check the influence of Language Style Matching (LSM) on people. For
instance, check LSM amongst strangers and find out how this changes as these
strangers spend more time with each other.

### Parts of project

There are two parts of this project: 

 - [Django 1.6] web framework based on Python with [Heroku] deployment.
 - Offline data analysis with R.


### Installation

You need: 

 - [Django 1.6] 
 - [VirtualEnv]
 - [R] installed globally. A userful R tool is [Rstudio].

### Visualization

[project demo webpage]

 - Still in very early testing phase but here is a link. The visualization shows how a user's chat message changes with events on different dates. The chat message is about 12000 messages collected over 2 months on [Viber].
 - Based on a selected valid date and event, you can view how a user's message changes.
 - Some of the visualizations will be replaced (read: pie chart).

### Process
 - Load location data from moves mobile app (complex nested json format) 
 - Load language style matching (lsm) data from EAF (csv format)
 - Find correlation between number of hours users spend together and their lsm starting with two users.


### Dataset

There are two types of input files: 

 - Moves data (json format): Moves data, from the mobile application, is used to obtain users' location data together with their activities.
 - LSM data (csv format): from an in-house project Email Analysis Framework (EAF). This is gmail data that gives the frequency density of functional words over total words. The functional words category used include: intensifier fraction, personal fraction,  lexical density, entropy, etc. More details below.

*NB: Dataset was removed to protect privacy.
*

### Details of linguistic features

 - Intensifier_fraction: The fraction of "intensifiers" (words such as "so","very","quite","rather","totally", etc.) over the total number of words. This is typically an indication of register: casual writing tends to have a higher number of intensifiers than formal writing.
 - Personal_fraction: The fraction of personal pronouns ("i","me","my","myself", etc.) over total words. This is again usually an indication of register.
 - Lexical_density: This is the ratio of "functional" (aka stopwords) versus "information-carrying" words in the sample. It's a useful statistic for determining if the text is "information dense", for example an excerpt from a textbook or a news article, versus an "information light" text like a conversation
 - iqv: This is one of the many indexes of qualitative variation, specifically the Gibbs M1 index. You can think of it as a measure of the "spread" of the distribution, that is the degree to which probability mass is concentrated in a few categories standardized to the total number of categories (in this case, words.)
 - Entropy: In the information-theoretic sense, entropy is average number of bits required to encode a sequence that contains repetitive subsequences. I included it on recommendation from Zhao et. al.'s 2006 paper, Using relative entropy for authorship attribution.
 - Counts: This is just the number of words that fall within the timeframe, used as a sanity check.

### Todo's

 - Write Tests
 - Add more documentation for Django project
 - Add more code Comments
 - Add more visualization

### References
 - [The Psychological Meaning of Words: LIWC and Computerized Text Analysis Methods]
 - [Language Style Matching Predicts Relationship Initiation and Stability] 
 - [The Psychological Meaning of Words: LIWC and Computerized Text Analysis Methods]: http://jls.sagepub.com/content/29/1/24.short
 - [Language Style Matching Predicts Relationship Initiation and Stability]:http://mollyireland.nfshost.com/Ireland...Pennebaker2011_PSci.pdf
 - [Moves App]: https://dev.moves-app.com/
 - [RStudio]: http://www.rstudio.com/products/rstudio/download/
 - [Django 1.6]: https://docs.djangoproject.com/en/1.6/topics/install/
  [VirtualEnv]: https://virtualenv.pypa.io/en/latest/
 [Heroku]: https://devcenter.heroku.com/articles/getting-started-with-django
 [R]: http://www.r-project.org/
[project demo webpage]:  https://partner-dyads.herokuapp.com/